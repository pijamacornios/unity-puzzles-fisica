﻿using System;
using niscolas.UnityUtils.Core;
using UnityEngine;
using UnityEngine.Events;

namespace PuzzlesFisica
{
    public class SimpleTrigger : CachedMonoBehaviour
    {
        [SerializeField]
        private UnityEvent _onTriggerEnter;
        
        private void OnTriggerEnter(Collider other)
        {
            _onTriggerEnter?.Invoke();
        }
    }
}
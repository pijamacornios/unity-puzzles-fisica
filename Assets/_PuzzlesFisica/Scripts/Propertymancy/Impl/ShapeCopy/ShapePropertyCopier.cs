﻿using System;
using System.Collections.Generic;
using System.Linq;
using Movement;
using niscolas.UnityExtensions;
using RotaryHeart.Lib.SerializableDictionary;
using UnityEngine;

namespace Propertymancy
{
    public class ShapePropertyCopier : PropertyCopier<CopyableShapeProperty>
    {
        [SerializeField]
        private SerializableDictionaryBase<ShapeType, GameObject> _visualByShapeType =
            new SerializableDictionaryBase<ShapeType, GameObject>();

        [SerializeField]
        private Transform _scalingTargetOverride;

        [SerializeField]
        private GroundDetectorBase _groundDetector;

        [SerializeField]
        private float _copyDuration = 0.5f;

        [SerializeField]
        private Rigidbody _rigidbody;

        private List<Collider> _selfColliders = new List<Collider>();

        private Action<float> _currentCopyColliderAction;
        private Action<float> _currentCopyVisualAction;
        private Quaternion _preCopyRotation;
        private bool _isCopying;
        private float _currentCopyTime;

        private void Start()
        {
            _selfColliders = GetComponents<Collider>().ToList();
        }

        private void Update()
        {
            if (!_isCopying)
            {
                return;
            }

            float t = Mathf.Clamp(_currentCopyTime / _copyDuration, 0f, 1f);
            _currentCopyColliderAction(t);
            _currentCopyVisualAction(t);

            _rigidbody.rotation = _preCopyRotation;
            _rigidbody.velocity = Vector3.zero;

            if (t >= 1)
            {
                _isCopying = false;
                return;
            }

            _currentCopyTime += Time.deltaTime;
        }

        protected override void Copy(CopyableShapeProperty copyableProperty)
        {
            if (_isCopying)
            {
                return;
            }

            _currentCopyColliderAction = CopyColliderData(copyableProperty);
            _currentCopyVisualAction = CopyVisualData(copyableProperty);
            CopyRigidbodyData(copyableProperty);

            _preCopyRotation = _rigidbody.rotation;
            _isCopying = true;
            _currentCopyTime = 0;
        }

        private void CopyRigidbodyData(CopyableShapeProperty copyableProperty)
        {
            _rigidbody.mass = copyableProperty.Mass;
            _rigidbody.drag = copyableProperty.Drag;
            _rigidbody.velocity = Vector3.zero;
        }

        private Action<float> CopyColliderData(CopyableShapeProperty copyableProperty)
        {
            DisableAllSelfColliders();

            switch (copyableProperty.ShapeType)
            {
                case ShapeType.Ellipsoid:
                    return GetCopyEllipsoidShapePropertyAction(copyableProperty);

                case ShapeType.Box:
                    return GetCopyBoxShapePropertyAction(copyableProperty);

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private Action<float> CopyVisualData(CopyableShapeProperty copyableProperty)
        {
            DisableAllVisuals();

            GameObject visual = _visualByShapeType[copyableProperty.ShapeType];
            visual.SetActive(true);

            Transform visualTransform = visual.transform;
            Vector3 initialScale = visualTransform.localScale;
            Vector3 endScale = copyableProperty.Scale;

            void CopyVisualAction(float t)
            {
                Vector3 currentScale = Vector3.Lerp(initialScale, endScale, t);
                UpdateScale(visualTransform, currentScale);
            }

            return CopyVisualAction;
        }

        private void UpdateScale(Transform currentVisualTransform, Vector3 scale)
        {
            if (_scalingTargetOverride)
            {
                _scalingTargetOverride.localScale = scale;
            }
            else
            {
                currentVisualTransform.localScale = scale;
            }
        }

        private void DisableAllVisuals()
        {
            SetAllVisualsState(false);
        }

        private void SetAllVisualsState(bool value)
        {
            _visualByShapeType.Values.ForEach(v => v.SetActive(value));
        }

        private Action<float> GetCopyEllipsoidShapePropertyAction(CopyableShapeProperty copyableProperty)
        {
            SphereCollider sphereCollider = GetSelfCollider<SphereCollider>();

            sphereCollider.enabled = true;
            sphereCollider.center = Vector3.zero;

            float initialColliderRadius = sphereCollider.radius;
            float endColliderRadius = copyableProperty.ColliderSize.x;

            void CopyColliderAction(float t)
            {
                sphereCollider.radius = Mathf.Lerp(
                    initialColliderRadius, endColliderRadius, t);
            }

            return CopyColliderAction;
        }

        private Action<float> GetCopyBoxShapePropertyAction(CopyableShapeProperty copyableProperty)
        {
            BoxCollider boxCollider = GetSelfCollider<BoxCollider>();

            boxCollider.enabled = true;
            boxCollider.center = Vector3.zero;

            Vector3 initialSize = boxCollider.size;
            Vector3 endSize = copyableProperty.ColliderSize;

            void CopyColliderAction(float t)
            {
                boxCollider.size = Vector3.Lerp(initialSize, endSize, t);
            }

            return CopyColliderAction;
        }

        private T GetSelfCollider<T>() where T : Collider
        {
            foreach (Collider selfCollider in _selfColliders)
            {
                if (selfCollider is T existingColliderImpl)
                {
                    return existingColliderImpl;
                }
            }

            T addedColliderImpl = gameObject.AddComponent<T>();
            addedColliderImpl.enabled = false;

            _selfColliders.Add(addedColliderImpl);

            return addedColliderImpl;
        }

        private void EnableAllSelfColliders()
        {
            SetAllSelfCollidersState(true);
        }

        private void DisableAllSelfColliders()
        {
            SetAllSelfCollidersState(false);
        }

        private void SetAllSelfCollidersState(bool value)
        {
            _selfColliders.ForEach(c => c.enabled = value);
        }
    }
}
﻿using niscolas.UnityUtils.Core;
using UnityEngine;

namespace Propertymancy
{
    public class CopyableShapePropertyLinkVisualizer : CachedMonoBehaviour
    {
        [SerializeField]
        private ShapePropertyCopier _shapePropertyCopier;

        [SerializeField]
        private LineRenderer _lineRenderer;

        private Transform _currentPropertyTransform;

        private void Start()
        {
            _lineRenderer.enabled = false;
            _shapePropertyCopier.CurrentCopyablePropertyChanged += ShapePropertyCopier_OnCurrentCopyablePropertyChanged;
        }

        private void Update()
        {
            if (!_currentPropertyTransform)
            {
                return;
            }
            
            _lineRenderer.SetPosition(0, _transform.position);
            _lineRenderer.SetPosition(1, _currentPropertyTransform.position);
        }

        private void ShapePropertyCopier_OnCurrentCopyablePropertyChanged(
            CopyableShapeProperty copyableShapeProperty)
        {
            if (!copyableShapeProperty)
            {
                _lineRenderer.enabled = false;
                _currentPropertyTransform = null;
                return;
            }
            
            _currentPropertyTransform = copyableShapeProperty.transform;
            _lineRenderer.enabled = true;
        }
    }
}
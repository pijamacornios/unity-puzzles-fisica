﻿using System.Linq;
using niscolas.UnityExtensions;
using niscolas.UnityUtils.Core;
using UnityEngine;

namespace Propertymancy
{
    public class CopyableShapeProperty : CopyableProperty
    {
        [SerializeField]
        private Rigidbody _rigidbody;

        public ShapeType ShapeType
        {
            get
            {
                Collider firstEnabledCollider = GetComponents<Collider>()
                    .FirstOrDefault(c => c.enabled);

                if (firstEnabledCollider is SphereCollider)
                {
                    return ShapeType.Ellipsoid;
                }
                else
                {
                    return ShapeType.Box;
                }
            }
        }

        public Vector3 ColliderSize => _extraColliderData.Size;

        public Vector3 Scale => _transform.localScale;

        public float Mass
        {
            get
            {
                if (_rigidbody)
                {
                    return _rigidbody.mass;
                }

                return 1;
            }
        }

        public float Drag
        {
            get
            {
                if (_rigidbody)
                {
                    return _rigidbody.drag;
                }

                return 0;
            }
        }

        private ExtraColliderData _extraColliderData;

        private void Start()
        {
            _gameObject.GetOrAddComponent(out _extraColliderData);
        }
    }
}
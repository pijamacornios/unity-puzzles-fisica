﻿using UnityEngine;

namespace Propertymancy
{
    [CreateAssetMenu(
        menuName = Constants.CreateAssetMenuPrefix + "Property Type",
        order = Constants.CreateAssetMenuOrder)]
    public class PropertyTypeSO : ScriptableObject { }
}
using System;
using niscolas.UnityUtils.Core;
using UnityEngine;

namespace Propertymancy
{
    public abstract class PropertyCopier : CachedMonoBehaviour
    {
        public abstract void CopyCurrent();
    }

    public abstract class PropertyCopier<T> : PropertyCopier
        where T : CopyableProperty
    {
        [SerializeField]
        private bool _copyOnCollision;

        [SerializeField]
        private bool _keepCopyableOnCollisionExit;

        [SerializeField]
        private bool _canCopySamePropertyMultipleTimes;

        public event Action<T> CurrentCopyablePropertyChanged;

        private T _currentCopyableProperty;
        private bool _currentIsCopied;

        private void OnCollisionEnter(Collision collision)
        {
            if (!collision.gameObject.TryGetComponent(out T copyableProperty))
            {
                return;
            }

            UpdateCurrentCopyableProperty(copyableProperty);

            if (_copyOnCollision)
            {
                CopyCurrent();
            }
        }

        private void UpdateCurrentCopyableProperty(T copyableProperty)
        {
            if (copyableProperty == _currentCopyableProperty)
            {
                return;
            }
            
            _currentCopyableProperty = copyableProperty;
            CurrentCopyablePropertyChanged?.Invoke(_currentCopyableProperty);
            _currentIsCopied = false;
        }

        private void OnCollisionExit(Collision other)
        {
            if (_keepCopyableOnCollisionExit ||
                !other.gameObject.TryGetComponent(out T copyableProperty) ||
                copyableProperty != _currentCopyableProperty)
            {
                return;
            }

            UpdateCurrentCopyableProperty(null);
        }

        public override void CopyCurrent()
        {
            if (!CheckCanCopy() || !_currentCopyableProperty)
            {
                return;
            }

            Copy(_currentCopyableProperty);
            _currentIsCopied = true;
        }

        private bool CheckCanCopy()
        {
            return _canCopySamePropertyMultipleTimes || 
                   !_canCopySamePropertyMultipleTimes && !_currentIsCopied;
        }

        public void Copy(Collider other)
        {
            GetGameObjectUtility.Delegate(other, Copy);
        }

        public void Copy(Collision collision)
        {
            GetGameObjectUtility.Delegate(collision, Copy);
        }

        public void Copy(GameObject otherGameObject)
        {
            if (!otherGameObject ||
                !otherGameObject.TryGetComponent(out T copyableProperty))
            {
                return;
            }

            Copy(copyableProperty);
        }

        protected abstract void Copy(T copyableProperty);
    }
}
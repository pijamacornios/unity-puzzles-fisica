﻿namespace Propertymancy
{
    public static class Constants
    {
        public const string CreateAssetMenuPrefix = "[" + SystemName + "]/";
        public const int CreateAssetMenuOrder = -100;
        public const string SystemName = "Propertymancy";
    }
}
﻿using Movement;
using niscolas.UnityUtils.Core;
using Propertymancy;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

namespace PuzzlesFisica
{
    public class InputHandler : CachedMonoBehaviour
    {
        [SerializeField]
        private Movement.Movement _movement;

        [SerializeField]
        private Jumper _jumper;

        [SerializeField]
        private PropertyCopier _propertyCopier;

        [SerializeField]
        private ReconnectableJoint _reconnectableJoint;

        public void Move(InputAction.CallbackContext callbackContext)
        {
            _movement.UpdateMovementDirection(
                callbackContext.ReadValue<Vector2>());
        }

        public void Jump(InputAction.CallbackContext callbackContext)
        {
            if (!callbackContext.ReadValueAsButton())
            {
                return;
            }
            
            _jumper.Jump();
        }

        public void Interact(InputAction.CallbackContext callbackContext)
        {
            if (callbackContext.ReadValueAsButton())
            {
                return;
            }
            
            _propertyCopier.CopyCurrent();
        }

        public void ResetLevel(InputAction.CallbackContext callbackContext)
        {
            if (callbackContext.ReadValueAsButton())
            {
                return;
            }

            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        public void HandleConnectionInput(InputAction.CallbackContext callbackContext)
        {
            if (callbackContext.ReadValueAsButton())
            {
                return;
            }
            
            _reconnectableJoint.ToggleConnection();
        }
    }
}
﻿using niscolas.UnityUtils.Core;
using UnityEngine;
using UnityEngine.InputSystem;

namespace PuzzlesFisica
{
    public class ForceKeyboard : CachedMonoBehaviour
    {
        [SerializeField]
        private PlayerInput _playerInput;

        [SerializeField]
        private string _controlScheme;

        private void Start()
        {
            _playerInput.SwitchCurrentControlScheme(_controlScheme, Keyboard.current);
        }
    }
}
﻿using niscolas.UnityUtils.Core;
using UnityEngine;

namespace Movement
{
    public class ConstantAddForce : CachedMonoBehaviour
    {
        [SerializeField]
        private Rigidbody _rigidbody;

        [SerializeField]
        private ForceMode _forceMode;

        [SerializeField]
        private Vector3 _force;

        private void FixedUpdate()
        {
            _rigidbody.AddForce(_force, _forceMode);
        }
    }
}
﻿using System;
using niscolas.UnityUtils.Core;
using UnityEngine;

namespace Movement
{
    public abstract class Movement : CachedMonoBehaviour
    {
        [SerializeField]
        protected float _speed;

        [SerializeField]
        protected float _minMovementMagnitude = 0.1f;
        
        protected Vector2 _movementDirection;

        public void UpdateMovementDirection(Vector2 movementDirection)
        {
            _movementDirection = movementDirection;
        }

        private void FixedUpdate()
        {
            if (!ShouldMove())
            {
                return;
            }
            Move();
        }
        
        protected abstract void Move();

        private bool ShouldMove()
        {
            return _movementDirection.magnitude >= _minMovementMagnitude;
        }
    }
}
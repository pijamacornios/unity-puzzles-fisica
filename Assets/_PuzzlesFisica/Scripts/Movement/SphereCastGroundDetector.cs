﻿using UnityEngine;

namespace Movement
{
    public class SphereCastGroundDetector : GroundDetectorBase
    {
        [SerializeField]
        private LayerMask _groundLayerMask;

        [SerializeField]
        private bool _useAutoSettings;

        [SerializeField]
        private Transform _originPoint;

        [SerializeField]
        private float _radius;

        [SerializeField]
        private float _maxDistance;

        private readonly RaycastHit[] _results = new RaycastHit[3];

        private void FixedUpdate()
        {
            int resultCount;
            Vector3 localScale = _transform.localScale;
            
            if (_useAutoSettings)
            {
                resultCount = Physics.SphereCastNonAlloc(
                    _originPoint.position,
                    localScale.magnitude * 0.5f,
                    Vector3.down,
                    _results,
                    0,
                    _groundLayerMask);
            }
            else
            {
                resultCount = Physics.SphereCastNonAlloc(
                    _originPoint.position,
                    _radius,
                    Vector3.down,
                    _results,
                    _maxDistance,
                    _groundLayerMask);
            }

            _isGrounded = false;

            for (int i = 0; i < resultCount; i++)
            {
                if (_results[i].collider.gameObject == _gameObject)
                {
                    NearestGroundPoint = _results[i].point;
                    continue;
                }

                _isGrounded = true;
                return;
            }
        }
    }
}
﻿using niscolas.UnityUtils.Core;
using UnityEngine;

namespace Movement
{
    public class Jumper : CachedMonoBehaviour
    {
        [SerializeField]
        private Rigidbody _rigidbody;

        [SerializeField]
        private Vector3 _jumpForce = Vector3.up;

        [SerializeField]
        private GroundDetectorBase _groundDetector;

        public void Jump()
        {
            if (!_groundDetector.IsGrounded)
            {
                return;
            }

            _rigidbody.AddForce(_jumpForce, ForceMode.Impulse);
        }
    }
}
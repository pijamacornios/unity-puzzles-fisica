﻿using UnityEngine;

namespace Movement
{
    public class RotatingMovement : Movement
    {
        [SerializeField]
        private Rigidbody _rigidbody;

        [SerializeField]
        private Vector3 _applyForcePosition;

        [SerializeField]
        private ForceMode _forceMode;

        protected override void Move()
        {
            Vector2 force = _movementDirection * _speed;
            Vector3 finalForce = new Vector3(force.x, 0, force.y);

            _rigidbody.AddForceAtPosition(
                finalForce,
                _transform.position + Vector3.Scale(_transform.localScale * 0.5f, _applyForcePosition),
                _forceMode);
        }
    }
}
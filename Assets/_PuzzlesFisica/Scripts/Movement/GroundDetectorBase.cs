﻿using niscolas.UnityUtils.Core;
using UnityEngine;

namespace Movement
{
    public abstract class GroundDetectorBase : CachedMonoBehaviour
    {
        [SerializeField]
        protected bool _isGrounded;

        public bool IsGrounded => _isGrounded;
        
        public Vector3 NearestGroundPoint { get; protected set; }
    }
}
﻿using System;
using UnityEngine;

public static class GetGameObjectUtility
{
    public static void Delegate(
        Collider collider, Action<GameObject> callback = null)
    {
        if (!collider)
        {
            return;
        }
        
        callback?.Invoke(collider.gameObject);
    }

    public static void Delegate(
        Collision collision, Action<GameObject> callback = null)
    {
        callback?.Invoke(collision.gameObject);
    }
}
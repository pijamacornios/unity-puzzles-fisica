﻿using niscolas.UnityUtils.Core;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace PuzzlesFisica
{
    public class LoadSceneComponent : CachedMonoBehaviour
    {
        [SerializeField]
        private string _sceneName;

        public void Do()
        {
            SceneManager.LoadScene(_sceneName);
        }

        public void Reload()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        public void Exit()
        {
            Application.Quit();
        }
    }
}
﻿using niscolas.UnityUtils.Core;
using UnityEngine;

namespace PuzzlesFisica
{
    public class TimeManager : CachedMonoBehaviour
    {
        [SerializeField]
        private float _initialTimeScale;

        protected override void Awake()
        {
            base.Awake();
            SetTimeScale(_initialTimeScale);
        }

        public void SetTimeScale(float value)
        {
            Time.timeScale = value;
        }
    }
}
﻿using UnityEngine;

namespace Fallcatcher
{
    public class CollisionTriggerFallCatcher : FallCatcherBase
    {
        private void OnTriggerEnter(Collider other)
        {
            Catch(other.gameObject);
        }
    }
}
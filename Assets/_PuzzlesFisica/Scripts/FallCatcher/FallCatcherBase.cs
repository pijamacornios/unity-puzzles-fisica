﻿using System;
using niscolas.UnityUtils.Core;
using UnityEngine;

namespace Fallcatcher
{
    public abstract class FallCatcherBase : CachedMonoBehaviour
    {
        [SerializeField]
        private Transform _respawnPoint;

        public void Catch(GameObject target)
        {
            target.transform.position = _respawnPoint.position;
        }
    }
}
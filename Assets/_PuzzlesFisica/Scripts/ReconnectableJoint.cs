﻿using System;
using System.Security.Cryptography;
using niscolas.UnityUtils.Core;
using UnityEngine;

namespace PuzzlesFisica
{
    public class ReconnectableJoint : CachedMonoBehaviour
    {
        [SerializeField]
        private Rigidbody _connectedBody;

        [SerializeField]
        private bool _startConnected;

        [SerializeField]
        private HingeJoint _hingeJoint;

        private void Start()
        {
            SetConnectedState(_startConnected);
        }

        public void SetConnectedState(bool value)
        {
            if (value)
            {
                Connect();
            }
            else
            {
                Break();
            }
        }

        public void Connect()
        {
            _hingeJoint = _gameObject.AddComponent<HingeJoint>();
            _hingeJoint.connectedBody = _connectedBody;
        }

        public void Break()
        {
            Destroy(_hingeJoint);
        }

        public void ToggleConnection()
        {
            SetConnectedState(!_hingeJoint);
        }
    }
}
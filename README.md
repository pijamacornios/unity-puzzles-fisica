# Simple Physics Based Puzzle Game

> 💡 Made for the Physics Programming class of the last semester of Game Design course in PUC MG

[![Presentation Video](https://img.youtube.com/vi/N3nUkmuiYWM/0.jpg)](https://youtu.be/N3nUkmuiYWM)